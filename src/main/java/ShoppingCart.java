import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class ShoppingCart {

    private final UserID userId;
    private final LocalDate creationDate;

    private final List<ShoppingCartItem> shoppingCartItems;

    public ShoppingCart(UserID userId, LocalDate creationDate) {
        this.userId = userId;
        this.creationDate = creationDate;

        shoppingCartItems = new LinkedList<>();
    }

    public UserID getUserId() {
        return userId;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void addItem(ProductID productId, int quantity) {
        shoppingCartItems.add(new ShoppingCartItem(productId, quantity));
    }

    public List<ShoppingCartItem> items() {
        return shoppingCartItems;
    }

    public BigDecimal getTotal() {
        return
            shoppingCartItems
                .stream()
                .map(item -> item.getProductId().getPrice().multiply(new BigDecimal(item.getQuantity())))
                .reduce(new BigDecimal(0), (accumulator, price) -> accumulator.add(price));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShoppingCart that = (ShoppingCart) o;
        return Objects.equals(userId, that.userId) &&
                Objects.equals(creationDate, that.creationDate) &&
                Objects.equals(shoppingCartItems, that.shoppingCartItems);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, creationDate, shoppingCartItems);
    }
}
