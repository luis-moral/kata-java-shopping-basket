public class ShoppingCartService {

    private final ShoppingCartRepository shoppingCartRepository;

    public ShoppingCartService(ShoppingCartRepository shoppingCartRepository) {
        this.shoppingCartRepository = shoppingCartRepository;
    }

    public void addItem(UserID userId, ProductID productId, int quantity) {
        shoppingCartRepository.getOrCreateForUser(userId).addItem(productId, quantity);
    }

    public ShoppingCart basketFor(UserID userId) {
        return shoppingCartRepository.getForUser(userId).orElseGet(null);
    }
}
