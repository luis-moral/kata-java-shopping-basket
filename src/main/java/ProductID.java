import java.math.BigDecimal;

public class ProductID {

    private final int id;
    private final BigDecimal price;

    public ProductID(int id, float price) {
        this.id = id;
        this.price = new BigDecimal(price);
    }

    public BigDecimal getPrice() {
        return price;
    }
}
