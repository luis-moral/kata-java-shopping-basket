import java.util.Objects;

public class ShoppingCartItem {

    private final ProductID productId;
    private final int quantity;

    public ShoppingCartItem(ProductID productId, int quantity) {
        this.productId = productId;
        this.quantity = quantity;
    }

    public ProductID getProductId() {
        return productId;
    }

    public int getQuantity() {
        return quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShoppingCartItem that = (ShoppingCartItem) o;
        return quantity == that.quantity &&
                Objects.equals(productId, that.productId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, quantity);
    }
}
