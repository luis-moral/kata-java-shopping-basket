import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class ShoppingCartRepository {

    private final Clock clock;
    private List<ShoppingCart> shoppingCarts;

    public ShoppingCartRepository(Clock clock) {
        this.clock = clock;

        shoppingCarts = new LinkedList<>();
    }

    public Optional<ShoppingCart> getForUser(UserID userId) {
        return
            shoppingCarts
                .stream()
                .filter(cart -> cart.getUserId().equals(userId))
                .findAny();
    }

    public ShoppingCart getOrCreateForUser(UserID userId) {
        return
            getForUser(userId)
                .orElseGet(() -> createAndAddShoppingCartForUser(userId));
    }

    private ShoppingCart createAndAddShoppingCartForUser(UserID userId) {
        ShoppingCart newShoppingCart = new ShoppingCart(userId, clock.now());
        shoppingCarts.add(newShoppingCart);

        return newShoppingCart;
    }
}
