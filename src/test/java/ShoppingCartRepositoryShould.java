import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;

@RunWith(MockitoJUnitRunner.class)
public class ShoppingCartRepositoryShould {

    private static final UserID ALICE = new UserID(1);
    private static final UserID CHARLIE = new UserID(2);
    private static final UserID BOB = new UserID(3);
    private static LocalDate CREATION_DATE = LocalDate.of(2018, 1, 10);

    private ShoppingCartRepository shoppingCartRepository;

    @Before
    public void setUp() {
        shoppingCartRepository = new ShoppingCartRepository(new TestableClock());
    }

    @Test public void
    create_a_new_basket_when_there_is_not_another_created_for_that_user() {
        ShoppingCart aliceFirstCallCart = shoppingCartRepository.getOrCreateForUser(ALICE);
        ShoppingCart aliceSecondCallCart = shoppingCartRepository.getOrCreateForUser(ALICE);

        Assertions.assertThat(aliceFirstCallCart).isEqualTo(aliceSecondCallCart);
        Assertions.assertThat(aliceFirstCallCart.getCreationDate()).isEqualTo(CREATION_DATE);
    }

    @Test public void
    be_unique_for_each_user() {
        shoppingCartRepository.getOrCreateForUser(ALICE);
        shoppingCartRepository.getOrCreateForUser(CHARLIE);

        ShoppingCart aliceShoppingCart = shoppingCartRepository.getForUser(ALICE).orElse(null);
        ShoppingCart charlieShoppingCart = shoppingCartRepository.getForUser(CHARLIE).orElse(null);
        ShoppingCart bobShoppingCart = shoppingCartRepository.getForUser(BOB).orElse(null);

        Assertions.assertThat(aliceShoppingCart).isNotNull();
        Assertions.assertThat(charlieShoppingCart).isNotNull();
        Assertions.assertThat(aliceShoppingCart).isNotEqualTo(charlieShoppingCart);
        Assertions.assertThat(bobShoppingCart).isNull();
    }

    private static class TestableClock extends Clock {
        public LocalDate now() {
            return CREATION_DATE;
        }
    }
}