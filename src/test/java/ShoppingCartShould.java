import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;

@RunWith(MockitoJUnitRunner.class)
public class ShoppingCartShould {

    private static final UserID ALICE = new UserID(1);
    private static final ProductID THE_HOBBIT = new ProductID(1, 5f);
    private static final ProductID BREAKING_BAD = new ProductID(2, 7f);
    private static final LocalDate TODAY = LocalDate.now();

    @Test public void
    contain_the_added_items() {
        ShoppingCart shoppingCart = new ShoppingCart(ALICE, TODAY);
        shoppingCart.addItem(THE_HOBBIT, 1);
        shoppingCart.addItem(BREAKING_BAD, 2);

        ShoppingCartItem hobbitItem = new ShoppingCartItem(THE_HOBBIT, 1);
        ShoppingCartItem breakingBadItem = new ShoppingCartItem(BREAKING_BAD, 2);

        Assertions.assertThat(shoppingCart.items()).containsExactly(hobbitItem, breakingBadItem);
    }

    @Test public void
    contain_the_creation_date() {
        ShoppingCart shoppingCart = new ShoppingCart(ALICE, TODAY);

        Assertions.assertThat(shoppingCart.getCreationDate()).isEqualTo(TODAY);
    }

    @Test public void
    calculate_the_total_amount() {
        ShoppingCart shoppingCart = new ShoppingCart(ALICE, TODAY);
        shoppingCart.addItem(THE_HOBBIT, 1);
        shoppingCart.addItem(BREAKING_BAD, 2);

        Assertions.assertThat(shoppingCart.getTotal()).isEqualTo(new BigDecimal(19f));
    }
}
