import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class ShoppingCartServiceShould {

    private static final UserID ALICE = new UserID(1);
    private static final UserID CHARLIE = new UserID(2);
    private static final ProductID SOME_PRODUCT = new ProductID(1, 5f);
    private static final ProductID ANOTHER_PRODUCT = new ProductID(2, 7f);

    @Mock
    private ShoppingCartRepository shoppingCartRepository;

    @Test public void
    create_new_basket_when_the_first_item_is_added() {
        Mockito
            .when(shoppingCartRepository.getOrCreateForUser(ALICE))
            .thenReturn(Mockito.mock(ShoppingCart.class));

        ShoppingCartService shoppingCartService = new ShoppingCartService(shoppingCartRepository);
        shoppingCartService.addItem(ALICE, SOME_PRODUCT, 1);
        shoppingCartService.addItem(ALICE, ANOTHER_PRODUCT, 1);

        Mockito
            .verify(shoppingCartRepository, Mockito.times(2))
            .getOrCreateForUser(ALICE);
    }

    @Test public void
    be_unique_for_each_user() {
        Mockito
            .when(shoppingCartRepository.getForUser(ALICE))
            .thenReturn(Optional.of(Mockito.mock(ShoppingCart.class)));

        Mockito
            .when(shoppingCartRepository.getForUser(CHARLIE))
            .thenReturn(Optional.of(Mockito.mock(ShoppingCart.class)));

        ShoppingCartService shoppingCartService = new ShoppingCartService(shoppingCartRepository);
        shoppingCartService.basketFor(ALICE);
        shoppingCartService.basketFor(CHARLIE);

        Mockito
            .verify(shoppingCartRepository, Mockito.times(1))
            .getForUser(ALICE);

        Mockito
            .verify(shoppingCartRepository, Mockito.times(1))
            .getForUser(CHARLIE);
    }
}
