import org.junit.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

public class ShoppingBasketFeature {

    private static final UserID USER_ANY = new UserID(1);
    private static final ProductID PRODUCT_THE_HOBBIT = new ProductID(10002, 5f);
    private static final ProductID PRODUCT_BREAKING_BAD = new ProductID(20110, 7f);
    private static LocalDate CREATION_DATE = LocalDate.of(2018, 1, 10);

    @Test public void
    should_contain_creation_date_added_items_and_total_price() {
        ShoppingCartService shoppingService = new ShoppingCartService(new ShoppingCartRepository(new TestableClock()));
        shoppingService.addItem(USER_ANY, PRODUCT_THE_HOBBIT, 2);
        shoppingService.addItem(USER_ANY, PRODUCT_BREAKING_BAD, 5);
        ShoppingCart shoppingCart = shoppingService.basketFor(USER_ANY);

        assertThat(shoppingCart).isEqualTo(expectedShoppingCart());
    }

    private ShoppingCart expectedShoppingCart() {
        ShoppingCart shoppingCart = new ShoppingCart(USER_ANY, LocalDate.now());
        shoppingCart.addItem(PRODUCT_THE_HOBBIT, 2);
        shoppingCart.addItem(PRODUCT_BREAKING_BAD, 5);

        return shoppingCart;
    }

    private static class TestableClock extends Clock {
        public LocalDate now() {
            return CREATION_DATE;
        }
    }
}
